import React, { useState } from 'react';
import "./DropdownSelect.scss";

export interface IDropdownProps {
    dropdownItems: IDropdownItem[];
    isMultiSelect: boolean;
    title: string;
}

export interface IDropdownItem {
    id: number;
    value: string;
}

export const DropdownSelect: React.FunctionComponent<IDropdownProps> = (props) => {
    const [selectedItem, setSelectedItem] = useState<IDropdownItem | null>(null);
    const [selectedItems, setSelectedItems] = useState<number[]>([] as number[]);
    const [isExpanded, setExpand] = useState<boolean>(false);

    const handleDropdownItemClick = (x: IDropdownItem) => {
        setSelectedItem(x);
        setExpand(false);
    }

    if (props.isMultiSelect) {
        return (
            <div className="dropdownContainer" onBlur={() => setExpand(false)} tabIndex={1}>
                <div className="dropdownHeader" onClick={() => setExpand(!isExpanded)}>
                    <div className="dropdownHeaderSelectedItems">
                        {selectedItems.length === 0 ?
                            props.title
                            :
                            selectedItems.map(x => <span className="dropdownHeaderSelectedItem" key={x}> {props.dropdownItems.find(y => y.id === x)!.value} </span>)
                        }
                    </div>
                    <span className="dropdownHeaderIcon"> <i className="fa fa-chevron-circle-down" aria-hidden="true" /> </span>

                </div>
                {isExpanded &&
                    <>
                        <ul className="dropdownItems">
                            {props.dropdownItems.map(x => {
                                return (
                                    <li key={x.id} className="dropdownItem" onClick={() => selectedItems.indexOf(x.id) === -1 ? setSelectedItems(selectedItems.concat(x.id)) : setSelectedItems(selectedItems.filter(y => y !== x.id))}>
                                        <span className="dropdownItemText">{x.value}</span>

                                        {selectedItems.indexOf(x.id) !== -1 && <i className="fa fa-check dropdownItemCheckbox" aria-hidden="true" />}
                                    </li>
                                )
                            })}
                        </ul>
                    </>
                }
            </div>
        );
    } else {
        return (
            <div className="dropdownContainer" onBlur={() => setExpand(false)} tabIndex={1}>
                <div className="dropdownHeader" onClick={() => setExpand(!isExpanded)}>
                    <span className="dropdownHeaderTitle">
                        {selectedItem ? selectedItem.value : props.title}
                    </span>
                    <span className="dropdownHeaderIcon">
                        <i className="fa fa-chevron-circle-down" aria-hidden="true" />
                    </span>
                </div>

                {isExpanded &&
                    <>
                        <ul className="dropdownItems">
                            {props.dropdownItems.map(x => {
                                return (
                                    <li key={x.id} className="dropdownItem" onClick={() => handleDropdownItemClick(x)}>
                                        <span className="dropdownItemText">{x.value}</span>
                                    </li>
                                )
                            })}
                        </ul>
                    </>
                }
            </div>
        )
    }
}