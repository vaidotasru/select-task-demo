import React from 'react';
import { DropdownSelect, IDropdownItem } from '../../components/DropdownSelect/DropdownSelect';
import { vegetables }  from '../../datasource/vegetables';

export const VegetableSelectContent: React.FC = () => {
    return (
      <div style={{width: '40vw'}}>
            <DropdownSelect
                isMultiSelect={false}
                dropdownItems={vegetables.map((x, i) => ({ id: i, value: x } as IDropdownItem))}
                title="Select a vegetable"
            />
      </div>
    );
  }