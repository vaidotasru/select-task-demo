import React from 'react';
import { DropdownSelect, IDropdownItem } from '../../components/DropdownSelect/DropdownSelect';
import { fruits } from '../../datasource/fruits';

export const FruitSelectContent: React.FC = () => {
  return (
    <div style={{width: '100vw' }}>
      <div style={{ margin: 0, width: '40vw'}}>
        <DropdownSelect
          isMultiSelect={true}
          dropdownItems={fruits.map((x, i) => ({ id: i, value: x } as IDropdownItem))}
          title="Select some fruits"
        />
      </div>
    </div>
  );
}