import React from 'react';
import { Router, Route } from 'react-router-dom';
import { createBrowserHistory } from "history";
import { VegetableSelectContent } from './features/vegetableSelect/VegetableSelectContext';
import { FruitSelectContent } from './features/fruitSelect/FruitSelectContent';

const history = createBrowserHistory();



const App: React.FC = () => {
  return (
    <div className="App">
      <Router history={history}> 
          <Route exact path="/"/>
          <Route path="/vegetables" component={VegetableSelectContent} />
          <Route path="/fruits" component={FruitSelectContent} />
      </Router>
    </div>
  );
}

export default App;